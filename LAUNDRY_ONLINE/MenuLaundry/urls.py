from django.urls import path
from . import views


urlpatterns =[
    path('MenuLaundry', views.MenuLaundry, name='MenuLaundry'),
    path('MenuLaundrys/', views.MenuLaundrys, name='MenuLaundrys'),

]
from django.apps import AppConfig


class InfolaundryConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'InfoLaundry'

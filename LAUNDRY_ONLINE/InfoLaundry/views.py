from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader




# Create your views here.
def InfoLaundry(request):
    template = loader.get_template('info.html')
    return HttpResponse(template.render())
